# laraveltools

#### 介绍
简单的api文档生成工具

适用于mvc方式，直接用闭包开发的没法生成


#### 安装教程

"require-dev": {
    "lj/laraveltools" : "^0.6"
}

#### 使用说明
1. php artisan vendor:publish
2. 选择 Jin\Laraveltools\ApiServiceProvider
3. 之后会自动复制 j-tools.php 到config目录

#### 配置说明
在j-tools.php 中配置 outFile 指向一个可写的目录文件


#### 注释示例
```
/**
     *
     * @jdoc /teacher 获取数据
     * @jdoc_group 用户组
     * @jdoc_param name string 用户名
     * @jdoc_param id int id
     *  -v required|numeric
     * @jdoc_param pid int pid
     *  -v #App\Models\Help::vv
     * @jdoc_response
     *  {
     *  "aa" : "bb"
     * }
     *
     */
```

#### 生成示例

php artisan j:api

#### 自动验证表单数据修改中。。。