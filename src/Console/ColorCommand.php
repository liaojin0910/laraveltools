<?php

namespace Jin\Laraveltools\Console;

use Illuminate\Console\Command;


class ColorCommand extends Command {

    public function yellow($msg) {
        $this->color($msg,"yellow");
    }

    public function red($msg) {
        $this->color($msg,"red");
    }
    public function green($msg) {
        $this->color($msg,"green");
    }
    public function blue($msg) {
        $this->color($msg,"blue");
    }
    public function black($msg) {
        $this->color($msg,"black");
    }

    public function white($msg) {
        $this->color($msg,"white");
    }

    public function magenta($msg) {
        $this->color($msg,"magenta");
    }

    public function cyan($msg) {
        $this->color($msg,"cyan");
    }

    public function color($msg,$color) {
        $out = sprintf("<fg=%s>%s</>" ,$color ,$msg);
        $this->line($out);
    }

}
