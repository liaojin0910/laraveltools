<html>
    <head>
        <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
        
    </head>
    <style>


        #main{
            display:flex;
            height:100%;
        }
        #left-div {
            flex:1;
            background-color:#bbbbbb;
        }

        #right-div {
            flex:5;
            background-color:#ffffff;
            padding-left: 20px;
        }
        .l-title {
            
            padding: 5px;
            cursor:pointer
        }

        .red{
            color: red;
        }

        .paramline {
            padding:5px;
            border-bottom: 1px dotted #bbbbbb;
        }

    </style>
    <body>

        <div id="main">
            <div id="left-div">
                <div @click="tClick(item)" v-for="item in items" :key="item.name" class="l-title">
                    @{{item.name}}
                </div>
            </div>
    
            <div id="right-div">
                <h3>@{{p.name}} </h3>
                <div>
                    <span>[@{{p.methods}}]</span>
                </div>
                <div>
                    @{{p.describe}}
                </div>
                
                <hr />
                <h3>参数列表</h3>
                <div v-for="param in p.params" class="paramline">
                    <div>@{{param.name}} 
                        <span class="red">@{{param.type}}</span> 
                        <span>@{{param.describe}}</span>
                    </div>
                </div>
                <hr/>

                <div>
                    <pre>

                    @{{p.response}}
                    </pre>
                    
                </div>
            </div>
        </div>

        
        

        <script type="text/javascript">

            let apidocs = JSON.parse("{!! $apidocs !!}");




            //console.log(Vue());
            var data = {
                "items" : apidocs,
                "p" : apidocs[0]
            }
            var app = new Vue({
                el: "#main",
                data : data,
                methods: {
                    tClick : function(item) {
                        this.p = item
                        
                    }
                }
            });

            

        </script>
    </body>
</html>
