<?php

namespace Jin\Laraveltools;

use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{

    public $commands = [
        Console\ApiCommand::class
    ];


    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {

        $this->registerPublishing();
        
    }



    /**
     * Register the package's publishable resources.
     *
     * @return void
     */
    protected function registerPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__.'/../config' => config_path()], 'j-tools');
        }
    }

 

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {


        $this->commands($this->commands);

    }

}
