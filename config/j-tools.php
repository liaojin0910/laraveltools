<?php

return [
    //暂时没应用
	"onlyDebug" => true,

    "validation" => [
        //开关验证功能
        "on" => true,
        //自定义，找个可写目录就行
        "file" => app()->storagePath().DIRECTORY_SEPARATOR."v.php",
    ],

    "tag" => "@jdoc",
    "groupTag" => "@jdoc_group",
    "responseTag" => "@jdoc_response",
    "hideTag" => "@jdoc_hide",
    "endTag" => "@jdoc_end",
    "paramTag" => "@jdoc_param",

    //找个可写目录
    "outFile" => app()->publicPath().DIRECTORY_SEPARATOR."apidoc.html",


    //自定义
    "error_message" => [
        'required' => '字段 :attribute 不能为空.',
        'date' => '字段 :attribute 日期格式错误了',
        'max' => '字段 :attribute 太长了',
        'email' => '字段 :attribute 要求是email格式',
        'numeric' => "字段 :attribute 必须是数字好不好? ",
    ],
];
